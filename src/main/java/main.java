import org.tempuri.Calculator;

/**
 *
 * @author Stefanus
 */
public class main {

    public static void main(String[] args) {
        int x = 5;
        int y = 3;
        int result = 0;
        Calculator calculator = new Calculator();
        
        result = calculator.getCalculatorSoap().add(x, y);
        System.out.println(x + " + " + y + " = " + result);
        
        result = calculator.getCalculatorSoap12().divide(x, y);
        System.out.println(x + " / " + y + " = " + result);
        
        result = calculator.getCalculatorSoap().subtract(x, y);
        System.out.println(x + " - " + y + " = " + result);
        
        result = calculator.getCalculatorSoap12().multiply(x, y);
        System.out.println(x + " x " + y + " = " + result);
    }
}
